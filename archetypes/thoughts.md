---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
description: 'As soon as Winston had dealt with each of the messages, he clipped his speakwritten corrections to the appropriate copy of the Times and pushed them into the pneumatic tube. '
image: /images/IMG_6772.jpg
draft: true
private: true
dot: "."
---
