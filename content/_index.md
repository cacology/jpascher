---
title: Do texts have a natural history
description: J. P. Ascher personal landing page
dot: "?"
---

Texts are, after all, produced by a form of life.

Scholarship about enlightened reading: bibliography, administration,
poetry, dictionaries, encyclopedias, news, history, and the long-tail
of the early-modern textual corpus.
