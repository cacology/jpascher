---
title: "Volumes of abridgments of the Transactions at US-icu"
date: 2022-08-30T08:20:24-05:00
description: 'Making sense of a binding by looking at several volumes'
image: v-5-spine.jpg
dot: "."
---

While John Lowthorp’s [abridgment of the *Philosophical Transactions
of the Royal Society of London*](/thoughts/us-icu-lowthorp) came first,
subsequent editors adopted the same organizational scheme and look.
Most copies of Lowthorp’s abridgment have several sibling volumes.

The University of Chicago Library [Hanna Holborn Gray Special
Collections Research Center](https://www.lib.uchicago.edu/scrc/) has
the second volume of the second abridgment by [Henry
Jones.](http://estc.bl.uk/T103708) It also has both volumes of the
third abridgment in the series by [John Eames and John
Martyn.](http://estc.bl.uk/T103705) Since these three separate works
cover 1665–1700, then 1700–1720, then 1719–1733, their title pages
have volume numbers in sequence.  Thus we have volumes one, five, six,
and seven.  To summarize, we have,

| John Lowthorp, 1665-1700                                         | Henry Jones, 1700-1720                                           | J. Eames and J. Martyn, 1719–1733                                  |
|:-----------------------------------------------------------------|:-----------------------------------------------------------------|:-------------------------------------------------------------------|
| Printed London, 1705,                                            | Printed London, 1721,                                            | Printed London, 1743,                                              |
| as volumes 1–3:                                                  | as volumes 4–5:                                                  | as volumes 6–7:                                                    |
|                                                                  |                                                                  |                                                                    |
| [Q41.L74 v.1]((http://pi.lib.uchicago.edu/1001/cat/bib/2445473)) | [Q41.L74 v.5]((http://pi.lib.uchicago.edu/1001/cat/bib/2445473)) | [Q41.L74 v.6–7]((http://pi.lib.uchicago.edu/1001/cat/bib/2445473)) |
|                                                                  |                                                                  |                                                                    |


In the picture above you can see the spine of volume five, lacking the
original leather.  The exposed backing material is a newspaper in
German.  Paul Stark, who seems to be selling musical instruments, was
active at the end of the nineteenth century.  The covers of the books
have a gilt stamp for the University of Chicago.  Here’s volume five,

![Cover of volume five](v-5-corner.jpg)

This is volume six,

![Cover of volume six](v-6-binding.jpg)

In both cases, we have badly worn morocco-grained institutional half
sheep over light olive brown German marbled paper with pale yellow
endpapers.  The edges are hair vein marbled.  The spine of volume
six has also been exposed,

![Spine of volume six](v-6-spine.jpg)

Dr. Graf seems to be a compounding pharmacist working in Berlin around
the end of the nineteenth century.  In both these cases, we have
institutional bindings for the University of Chicago, but newspaper
backing suggesting these bindings were late nineteenth-century German
ones, or at least used nineteenth-century German newspapers as scraps
to make the bindings.  The marbled endpapers support this as well,
though no where near as strongly as anyone could use the German-style
marbled paper.

Lastly, we have a bookplate that has an important clue,

![Bookplate of volume five](v-5-bookplate.jpg)

That the handwriting is similar to the [numbers on volume
one](/thoughts/us-icu-lowthorp). These volumes also have similar
numbers written later on.  Those numbers are in sequence with those
from the first volume.  The plate records the book’s classification in
the Dewey decimal system.  Number “062” refers to [Organizations in
the British Isles or
England.](https://www.library.illinois.edu/infosci/research/guides/dewey/)
The University of Chicago libraries do not currently use Dewey’s
decimal system, but this bookplate certainly does not look modern.

Note too that the book was given by “Calvary.”  Not a first name and
last name, but something or some group named Calvary.

Thus we have several volumes of abridgments in what seem to be
nineteenth-century German bindings made for the University of Chicago.
It is still not terribly clear yet [how these scattered volumes got to
North America.](/thoughts/us-icu-lowthorp)
