---
title: "Basic bibliographical observation"
date: 2020-08-26T09:32:53+01:00
description: 'I think of bibliography as primarily a method, rather than an aim. '
dot: "."
---

Bibliography as I study it is more of a method, rather than an aim.
It can be applied to various objects from various time periods and
various cultures.  My research tries to show some of the wide
application of this method, but here are some preliminary talks I gave
on what I think is the core of the method:

## Basic Bibliographical Observation, Part 1

{{< youtube id="xynEIZWayJA" >}}

## Basic Bibliographical Observation, Part 2

{{< youtube id="LthrWF_-_q4" >}}

## Basic Bibliographical Observation, Parts 3–4

I’ve written these, but because I’ve had to pack up my Charlottesville
office to be an unhoused scholar, I’ve not recorded them.  I’d like to
and encouragement is welcome!  Send me an email if you think they’d be
useful for you, or if you have a venue.
