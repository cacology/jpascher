---
title: "Indigenous peoples contributions to early natural history"
date: 2022-01-07T07:46:58+01:00
description: 'A talk from MLA 2022, I try to understand a bibliographical methodology that enables us to recover the contributions of indigenous people to the Philosophical Transactions'
dot: "."
---

There’s a great deal of work done on how different communities of
North American people contributed to the development of natural
history, philosophy, and science.  I use a combination of
bibliography, chronology, and careful reasoning to understand the
nature of a single report in the *Philosophical Transactions* of the
Royal Society of London.

My talk at the MLA 2022 panel [“Doing Editing Differently”](https://mla.confex.com/mla/2022/meetingapp.cgi/Paper/18602) was not recorded, but you can seem my slides and follow
along with my talk script if you’d like to read it.

## Slides

{{< embed-pdf url="/thoughts/indigenous-peoples-contributions/Slides.pdf" >}}

[Slides](Slides.pdf)

## Talk script

[Talk script](talk-text.pdf)

It’s a refinement of the talk I give [in this earlier post.](/thoughts/editing-submerged-voices/)
