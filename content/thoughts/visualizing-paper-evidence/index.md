---
title: "Visualizing paper evidence"
date: 2017-05-25T08:10:03+01:00
description: "When we look at books through digital screens, we can sometimes see more with the careful use of visualizations."
image: "header.png"
dot: "."
---

I wrote this piece a few years back where I argue that “Digital images
both lie to us and tell us truths that exist outside of our normal
perception. The lie comes about through both deliberate distortions
and distortions produced by limitations in digital and in other
reproduction methods. The limitations of reproductions are easy to see
for anyone who considers the situation carefully, but understanding
the problems that they create in our understanding of the past
requires some study.”

The piece shows how we can use high-quality digital images to recover
paper stocks used to print a book and analyze its structure:

[Visualizing Paper Evidence Using Digital Reproductions](https://scholarslab.lib.virginia.edu/blog/visualizing-paper-evidence-using-digital-reproductions/)

Since that time, however, I have prepared a public-facing
OpenSeaDragon version of the image itself.  You can zoom in from
looking at the whole book at once to examining individual pieces of
type.  I recommend reading the article above and then looking at this
zoom to see what you can spot:

[Big zoom](http://slab.space/~jpsa/big-zoom.html)
