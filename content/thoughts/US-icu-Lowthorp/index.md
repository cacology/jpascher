---
title: "Lowthorp’s abridgment at US-icu"
date: 2022-08-16T08:20:24-05:00
description: 'Examining the features of a particular copy of the abridgment of the Philosophical Transactions at the University of Chicago Library Hanna Holborn Gray Special Collections Research Center'
image: book-plate.jpg
dot: "."
---

John Lowthorp abridged the *Philosophical Transactions of the Royal
Society of London* between 1703 and 1705, but the abridgment was
subsequently reprinted over and over.  The fifth numbered edition of
1749 was printed in 500 copies according to the Bowyer ledgers.  There
are only seven copies of the 1705 edition in North America, since the
early editions mostly went to people closely connected with the Royal
Society and stayed in London.

The copy of volume one of the 1705 abridgment at the University of
Chicago Library [Hanna Holborn Gray Special Collections Research Center](https://www.lib.uchicago.edu/scrc/)
([US-icu Q41.L74
v.1](http://pi.lib.uchicago.edu/1001/cat/bib/2445473)) repays careful
attention.  Why is it in North America?  Why Chicago?  When did it get
here?  Why this edition?

What do we make of the [guide numbers on the plates](/thoughts/us-icu-guide-numbers)?  Why does this have an early state?

The bookplate, above, is lithographically printed in red and
relatively modern.  Penciled on it and the upper pastedown endpaper is
“Gen. Lib.”  The hinges, between the cover and the book block above,
are cloth and have been renewed.  This is not a binding from 1705, but
a repair of a binding somewhat later.  Book cloth was not used until
the early nineteenth century and infrequently used in repairs like
this until the [twentieth
century](https://cool.culturalheritage.org/don/dt/dt0414.html).

We thus say the binding is “modern,” meaning modern to this book.

On the back of the title page and on the first page of text, we have some markings:

![Title page Verso and contents recto](numbers.jpg)

In the upper left in pencil, you can see the present call number “Q41
\| .L74 \| v.1 \| c.2 \| Rare”.  The number essentially matches the
electronic record maintained by the library now.  On the right, the
recto, begins the dedication in the text, but the page also has two
numbers.  On the top in brown ink, library hand is “99931” and on the
bottom in a light greenish blue is stamped “230098.”

Libraries in the United States during the twentieth century typically
make marks like these.  They probably have something to do with the
history of the libraries at the University of Chicago.

We can [examine several other volumes to get more evidence.](/thoughts/us-icu-lowthorp-volumes)
