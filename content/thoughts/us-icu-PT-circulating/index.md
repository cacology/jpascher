---
title: "Circulating copies of the Transactions from the Calvary bookshop"
date: 2022-09-19T08:20:24-05:00
description: 'The Calvary bookshop’s stock continued to accrue history as it was used'
image: cart.jpg
dot: "."
---

I’ve been puzzling through [the source of copies of the Transactions
and its abridgments](/thoughts/us-icu-lowthorp-calvary) at he
University of Chicago Library [Hanna Holborn Gray Special Collections
Research Center](https://www.lib.uchicago.edu/scrc/).  Some of the
copies remained in the circulating collection and ultimately ended up
in [Joe and Rika Mansueto
Library](https://www.lib.uchicago.edu/mansueto/), the university’s
research collection.

To recap, the new university President bought the S. Calvary and
Company’s entire book collection to establish the library.  Over time,
some volumes were withdrawn and others moved from one collection to
another.

I’m grateful to [Seth
Vanek](https://www.lib.uchicago.edu/about/directory/staff/seth-vanek/)
who helped me page a cart of books that you can see above.  These are
the volumes listed in the [accession records that have not been
withdrawn](/thoughts/us-icu-lowthorp-accession).  They have a variety
of different marks, but every single one has the hand-written
accession number that places it within this initial acquisition from
the S. Calvary and Company purchase.

Most of the volumes have been rebound in the 1940s or ’60s but a few
remain in repaired bindings enclosed in boxes.  The volumes have a
whole history of the collection and institution written on them.
Consider these circulation cards at the back of [volume
128](http://pi.lib.uchicago.edu/1001/cat/bib/1015306).

![Circulation cards of volume 128](card-v128.jpg)

When it was checked-out, one of the two cards must have stayed in a
file while the other stayed with the book.  The white card has a stamp
saying it was at the bindery in 1945 and 1949.  It might have been
stamped at the bindery.  Other volumes have the a number stamped on
this card too.  Volume 120 has the number “220468” stamped on the card
and on the first page of text.

![Volume 120 card](card-v120.jpg)

And on the recto of the leaf after the title page, you can see the
same number.

![Volume 120 numbers](v120-numbers.jpg)

Since the card includes both the accession number and this new number,
it must not be a replacement number, but a different record.  Based on
the presence on a card and the book, I’d guess it was a number for a
circulation file.  I’ll summarize some marks on the book in a [table
like I used before](/thoughts/us-icu-lowthorp-accession/).

| Vol. 115 (1825)                                    | 117 (1827)                                   | 118 (1828)  | 120 (1830)    | 121 (1831) | 126 (1836)  |
|:---------------------------------------------------|:---------------------------------------------|:------------|:--------------|------------|-------------|
| [modern blue cloth (mbc)](mbc-v115.jpg),           | mbc                                          | mbc         | mbc           | boxed, mbc | mbc         |
| rebound on: [22 Dec  1948](binding-v115.jpg)       |                                              | 20 Jan 1964 | 24 April 1945 |            |             |
|                                                    |                                              |             |               |            |             |
| Bookplate: [Institutional (I)](bookplate-v115.jpg) | [Berlin Collection (BC)](bookplate-v117.jpg) | I           | I             | BC         | BC          |
|                                                    |                                              |             |               |            |             |
| Accession: 99944                                   | 99947                                        | 99948       | 99950         | 99951      | 99956       |
|                                                    |                                              |             |               |            |             |
| Stamped:  193349                                   | 162746                                       | 220467      | 220468        | 217480     | 217481      |
|                                                    |                                              |             |               |            |             |
| Barcode: 15 407 700                                | 098 732 594                                  | 098 878 049 | 098 878 162   | 73 407 500 | 098 878 340 |
|                                                    |                                              |             |               |            |             |
| Peciled on t.p.: [9843](pencil-v115.jpg)           | 9843                                         | 9843        | 9843          | 9843       | 9843        |
|                                                    |                                              |             |               |            |             |
|                                                    |                                              |             |               |            |             |

----

| Vol. 127 (1837)        | 128 (1838)             | 129 (1839)   | 130 (1840)  | 131 (1841)                                                               | 132 (1842)                                                                                                |
|:-----------------------|------------------------|--------------|-------------|--------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------|
| mbc                    | mbc                    | mbc          | mbc         | [boxed, repaired 19c Ger. binding typical of this set](binding-v131.jpg) | [boxed, repaired 19c Ger. binding with differently marbled papers from the rest](binding-v132.jpg) |
|                        | 28 June 1945, Oct 1949 | 11 June 1945 |             |                                                                          |                                                                                                    |
|                        |                        |              |             |                                                                          |                                                                                                    |
| Bookplate: BC          | BC                     | I            | BC          | BC                                                                       | I                                                                                                  |
|                        |                        |              |             |                                                                          |                                                                                                    |
| Accession: 99957       | 99958                  | 99959        | 99960       | 99961                                                                    | 99962                                                                                              |
|                        |                        |              |             |                                                                          |                                                                                                    |
| Stamped: 158662        | 29720                  | 215544       | 163794      | 25743                                                                    | 198920                                                                                             |
|                        |                        |              |             |                                                                          |                                                                                                    |
| Barcode: 098 878 405   | 34 730 059             | 098 878 463  | 098 878 057 | 73 407 771                                                               | 19 771 945                                                                                         |
|                        |                        |              |             |                                                                          |                                                                                                    |
| Penciled on t.p.: 9843 | 9843                   | 9843         | 9843        | 9843                                                                     | 9843                                                                                               |
|                        |                        |              |             |                                                                          |                                                                                                    |
|                        |                        |              |             |                                                                          |                                                                                                    |

----

Just this small sample shows us several things.  The books were mostly
rebound in the ’40s, but also the in ’60s.  One in a repaired Calvary
binding was checked-out during the 1940s, so didn’t get rebound.  The
rebinding thus must have progressing systematically through the
shelves, not as the books wore out.

The stamped numbers range from the 25,000s to 220,000s, but none are
in any particular sequence other than volumes 121 and 126.  It seems
like these were not systematically assigned to the volumes.  If they
are indeed related to circulation, it seems plausible that they were
assigned when the book was checked out.  Perhaps the circulation desk
had a stamp and stamped both the file card and the book when it was
checked out for a period of decades?  Volume 117 does not have the
number stamped on the card, but its [cards are also typed and do not
list anyone checking it out.](card-v117.jpg)  Perhaps these are
replacement cards?

The bar codes seem to have three ranges, those beginning with “098”
and those beginning only two digits.  Like the stamped numbers, they
are not sequential, so seem to have been assigned as the items
circulated, or perhaps returned.

The bookplates come in two forms, one I call
[“institutional”](bookplate-v115.jpg) because it merely names the
university and one I call [“Berlin Collection”](bookplate-v117.jpg)
because it marks the books as part of that collection.  Only volume
128 has a note of a visit to the bindery and the Berlin Collection
bookplate.  The rest of the volumes with a note about visiting the
binding have an institutional bookplate.  And the rest with Berlin
Collection bookplates don’t have a note.  Volume 128 is also odd in
that it is recorded visiting the bindery twice.  Possibly the bindery
did not have the Berlin Collection bookplates, but maybe received some
in 1948?  We’d need to see more volumes to figure this out.

The [abridgments](/thoughts/us-icu-lowthorp) all have “9842” penciled
on the title page.  This suggests that this number records one of two
things.  They could record the books as they arrive at the university
in batches; or they record the stock in the bookshop.  Since the
library accessioned by volume according to the prevailing standards
for librarianship in 1892, it seems unlikely that they would have also
numbered them in this irregular way shortly before then. Bookshops
often put the price of a book on a title page, which suggests that
this is a stock number for the bookseller rather than a mark from the
library.

Perhaps there is a catalog of the stock of S. Calvary and Company?
