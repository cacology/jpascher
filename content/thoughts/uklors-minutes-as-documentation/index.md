---
title: "Minutes at the Royal Society as evidence of publication"
date: 2022-10-25T08:20:24-05:00
description: 'The Royal Society documented their own publication activities if you know how to look'
image: Lowthorp-5-may-1703-jbo-11.jpg
dot: "."
---

In addition to printed collections, Royal Society has [large,
manuscript collections](https://royalsociety.org/collections/) that
document its own dissemination of knowledge.  Charles II granted them
a special patent in 1662 that allowed them to disseminate knowledge.
The minutes of the meetings authorize the use of this patent early on
and become habitual over time.

The minutes provide detailed records of the early-modern world’s
development of [administrative
law](https://en.wikipedia.org/wiki/Administrative_law), or [*droit
administratif*](https://fr.wikipedia.org/wiki/Droit_administratif), as
it applies to copyright and publication.

The minutes outline John Lowthorp’s interaction with the society to
navigate the obligations to produce his [1705 abridgment of their
*Philosophical Transactions* to 1700](http://estc.bl.uk/T103703).
Above you can see the order in the *Journal Book* from 5 May 1703
([JBO/11](https://catalogues.royalsociety.org/CalmView/Record.aspx?src=CalmView.Catalog&id=JBO%2f11&pos=1)/30,
p. 21), in which Lowthorp presents his proposal and a specimen.  You
can see his approval as well.

What exactly happened at this meeting?  As anyone who has ever taken
or read minutes knows, the final record distills a great deal of
activity.

## How did they take notes?

The procedures of Parliament and the House of Commons would have been
a obvious model for these meetings.  [Orlo Cyprian
Williams](http://explore.bl.uk/BLVU1:LSCOP-ALL:BLL01003935488)
explains that business was presented orally and that the clerk of the
commons took notes.  These notes allowed the clerk to remember what
had occurred at previous meetings, but also to read past resolutions
aloud when asked.  The notes would state the question as it had been
proposed and the resolution.  In 1547 Clerk John Seymour began to keep
these notes in consecutive order in a small, quarto book with his name
on it.

The next clerk, Fulk Onslow, also kept notes in the same way.  But in
1571 Onslow introduced

> a new technique in compiling his Journal, writing a second or fair
> copy from the rough notes scribbled in the House, and at the same
> time changing its format from a small disheveled quarto, 6" × 8", to
> a neat and orderly folio, 8" × 13". (qtd. Williams, 17)

Successive clerks continued Onslow’s style of record.  By the
Restoration of Charles II in 1660, the *Journals* of the House of
Commons were a well known authority on what happened.  Many members of
the early Royal Society would have seen this style of record keeping
personally.

Thus the Royal Society had a secretary who kept a journal, read past
minutes aloud, presented business orally, and recorded new business.
Like in the Commons, members would bring in petitions to read aloud
and then hand their copy to the secretary or clerk for the record.

## John Lowthorp’s proposal and its documentation

![Excerpt from Lowthorp’s proposal](proposal-clp-22i-59-p4.jpg)

John Lowthorp brought in a four-page manuscript proposal that he read
aloud.  The spoken proposal smoothed out the roughness of the written
document’s corrections.  The spoken document itself was the business,
so no fair copy was made.  The written document remained as a record
for the secretary’s reference, but ultimately ended up in the
Classified Papers of the modern Royal Society’s collections
([CLP/22i/59](https://catalogues.royalsociety.org/CalmView/Record.aspx?src=CalmView.Catalog&id=CLP%2f22i%2f59))

The secretary, however, recorded the day’s events in a [small vellum-bound book](ms-578-cover.jpg) in rough minutes ([MS/578](https://catalogues.royalsociety.org/CalmView/Record.aspx?src=CalmView.Catalog&id=MS%2f557&pos=1)).

![MS/578 spread including 5 May 1703](ms-578-5-may-1703-spread.jpg)

In the lower right, the paragraph beginning “Mr Lowthorp also
presented a Proposal” describes the event as it occurred.  Notice the
pause before “Approv’d,” which suggests that the first part was
written out, a discussion occurred, and a vote was taken to approve.

Across the spread, the two different styles of handwriting attest to
the two possibilities: these were written in slightly different
scenarios such as on a lap versus on a table, or these were written by
two different people.  Either would be unremarkable as there were
various secretaries, these were rough notes to be copied later, and
because the secretary employed various clerks and amanuenses who were
paid to copy texts.

Note too that there are corrections throughout the minutes.  From the
records, it seems that sometimes the minutes were read aloud at the
next meeting, revised, and approved.  An example from 4 January 1698/9
mentions a revision.  (JBO/10, p. 94)

![JBO/10 4 January 1698/9](jbo-10-p94.jpg)

Another example mentions that the secretary had fallen behind in
copying the minutes out of their notes and into the journals.

![JBO/10 19 April 1699](jbo-10-p117.jpg)

So the final version of the notice of Lowthorp’s proposal reflects not
minutes written to the moment, but minutes possibly revised, written
sometime later, possibly by a different person.  Thus when we read the
minute at the top of this webpage, what we have is not a document of
an event, but a document of a social consensus of what they would
treat as having happened at an event.

## The parliamentary versus banking model of minutes

The establishment of a social consensus at the Royal Society functions
slightly differently than the journal for the House of Commons.  On
the one hand, the journals of the House of Commons represent the
authorized activities of government.  The social consensus around
their content establishes the results of the King in Parliament.  Thus
only certain authorized people and methods can be used to establish
the authentic record.

The Royal Society, on the other hand, means to document both their
legal use of certain delegated privileges and to provide a record of
knowledge and errors along the way.  Their journals represent
accumulated wisdom and I think a [metaphor of
banking](http://jpascher.org/thoughts/commonplace-as-bank/) is a
better way to think of them.  Like a shared commonplace book, or like
the journals of a scrivener that copies manuscript separates, the
purpose of their journals is to collect the raw details into one
place.  At a later date, they can distill them into indices so those
details can be reused.

This explains the survival of the rough and draft minutes for the
Royal Society.  In a banking model, these would be the memorandum
book, or waste book, that could be used to recreate the journal.
Their journals represent a deliberately constructed, linear account,
of what happened, but acknowledge their debt to the original notes.
