---
title: "ESTC records as evidence"
date: 2022-06-08T23:28:52+01:00
description: 'Reading thousands of ESTC records to answer questions about the history of printing.'
image: ESTC-records.png
dot: "."
---

I’ve been trying to understand how a seventeenth-century bookseller
signals that they’re using a special privilege to print a book.
Somehow, the book must have signaled to a member of the Stationers’
Company that the bookseller had the appropriate rights, even if they
were not in the Register Book.  Otherwise, printers would have to keep
coming by and asking to see your privilege.  Perhaps they just knew?
Or, perhaps there was a parallel record of some sort?

I hypothesize that the imprint itself noted the special situation.
Those printing for the King directly might style themselves “printer
to the King,” so maybe the phrase “printer to” signals a source of
authority.

Could “printer to the Royal Society,” for example, signal that a book
was printed under the society’s charter and license?

One half of this is easy enough to check, do the books authorized
under the society’s charter always say “printer to the Royal Society”
in the imprint?  It just required looking at 1,194 books over a period
of about two days.  The technical details are a bit tedious, so I’ll
link them below, but the high-level approach is straightforward:

1. **First**, download the records from the [English Short Title
   Catalogue](https://estc.bl.uk) for each known printer to the Royal
   Society and any records that mention the “Royal Society” at all.
2. **Secord**, convert them to a record format.  I use [GNU
   Recutils](https://www.gnu.org/software/recutils/) because it’s
   easy.
3. **Third**, go through the records, one at a time, and add a new field for the
   authority under which a book was printed and the booksellers who
   undertook that printing.
4. **Fourth**, use your structured data to look for authorized printing and make
   charts.  Here’s one:
   
![Printers to the Royal Society](Chart-of-imprints.png "Chart of the printers to the Royal Society")

Furthermore, of the 1,194 records I looked at, only one book was
authorized by the Royal Society, but did not say “to the Royal
Society,” or equivalent in Latin, or abbreviated.  There were several
broadsides and lists of members that lacked the “to the” phrasing, but
licensing for those worked differently.

There is one potential shortcoming of this approach.  In step three, I
relied on my judgment to decide the authority and the printer.
Imprints can lie about who printed them.  More to the point, if the
book isn’t licensed by the Royal Society, but looks like it is, I
wouldn’t have been able to tell.  I think errors of inclusion are
small, however, since I spend a lot of time reading the minutes of the
Council and am used to the sorts of things the society authorized.

The improved study would use the list of printing ordered in the
Council minutes to find out if things that looked authorized really
were, and to find out if something authorized fell outside of normal
patterns or channels.

Yet, I think this basic approach could work well with other questions
that can use the evidence recorded by the ESTC.

The [technical
description](https://gitlab.com/cacology/estc-record-processing)
expects you to understand the basics of [GNU
Recutils](https://www.gnu.org/software/recutils/), [GNU
Emacs](https://www.gnu.org/software/emacs/),
[Elisp](https://www.emacswiki.org/emacs/LearnEmacsLisp), and [GNU
Bash](https://www.gnu.org/software/bash/), but all four of those can
be read about on a wide variety of websites.  Understanding how to
reproduce this work might make a good introduction.

This research is included paper I’m giving at the upcoming [CHAPTER
Conference 2022](https://chapter2022.uk).
