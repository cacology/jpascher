---
title: "Abridgments of the Transactions at the Royal Society"
date: 2022-10-18T08:20:24-05:00
description: 'The abridgments presented by Fellows to the Society have exceptional evidential value'
image: shelf.jpg
dot: "."
---

The Royal Society has [large, printed
collections](https://royalsociety.org/collections/) that document its
own dissemination of knowledge.  John Lowthorp’s [1705 abridgment of
their *Philosophical Transactions* to 1700](http://estc.bl.uk/T103703)
sits within a [series of sibling
works](https://catalogues.royalsociety.org/CalmView/Record.aspx?src=CalmView.CatalogL&id=18457&pos=1)
in that collection.  Although the works in the series were produced
over a period of fifty years, the series now has uniform bindings.  In
the image above, you can see the modern bindings with red lettering
pieces on the spine.

Where did these volumes come from?  Why are they now bound the same?

I’ll summarize the evidence from the volumes and the archives at the Society.

The second work in the series, the [abridgment by Henry Jones from
1700 to 1720](http://estc.bl.uk/T103708), comprises volumes four and
five because it continued the work begun by John Lowthorp in volumes
one, two, and three.  The numbering in the image above reflects the
title pages of the works.  The title page to Jones’s volume also has
writing across the top identifying the donor.

![Volume four title page spread](tp-v4.jpg)

It reads “Liber Regalis Societatis Ex dono Aut[h]oris. \| 8o. die
Januarii 1721/2” (The Royal Society’s book, given by the author, 8
January 1722).  Volume five has the same inscription.  The minutes of
the Society do not note Jones’s gift at their meeting on 11 January,
so he must have presented it directly to the collection.

The next two volumes, six and seven, were [prepared by John Eames and
John Martyn.](http://estc.bl.uk/T103705) While they do not inscribe
anything on their copy, Martyn presents it at a meeting on 13 June
1734 and is thanked.

> Mr Martyn made a present of the abridgement of the Philosophical
> Transactions from the year 1719 to 1733, in two Volumes in quarto,
> compiled by Mr John Eames and himself.

> For which present the thanks of the Society were ordered. [(JBO/15,
> p. 444)](https://catalogues.royalsociety.org/CalmView/Record.aspx?src=CalmView.Catalog&id=JBO%2f15)

The next three volumes above—eight part one, eight part two, and
nine—were [prepared by John Martyn alone.](http://estc.bl.uk/T103701)
The copy held by the Royal Society is inscribed by [Thomas Frederick
Colby
(1784–1852)](https://catalogues.royalsociety.org/CalmView/Record.aspx?src=CalmView.Persons&id=NA3821&pos=1),
who was born after they had been completed.

![Inscription on volume eight](inscription-v8.jpg)

These volumes, eight and nine, do not appear in the 1836 [catalogue
prepared by the Society’s librarian Anthony
Panizzi](https://wellcomecollection.org/works/uqr8bhnq/items?canvas=604),
but the earlier ones do.  A single part of volume ten [also
appears](https://wellcomecollection.org/works/uqr8bhnq/items?canvas=605).
The 1881 catalogue [lists these volumes along with those now inscribed
by Major
Colby.](https://hdl.handle.net/2027/coo1.ark:/13960/t9b574d6p?urlappend=%3Bseq=75)

Volumes one through three are not inscribed, but are printed on larger
than usual paper.  John Lowthorp’s printed prospectus explains,

> The Book shall be Printed on the same Paper [as this proposal], and
> with the same letter[forms] that this proposal is done with, except
> for some few on large paper for such gentlemen as are
> curious. ([ESTC T142498](http://estc.bl.uk/T142498), p. 3)

The British Library holds Hans Sloane’s copy ([Uk 740 h. 5:
v. 3](http://explore.bl.uk/BLVU1:LSCOP-ALL:BLL01002243373)) which is
also on large paper.  The “gentlemen as are curious” presumably refers
to people closely associated with the Society who were willing to
sponsor a more substantial version.  The presentation of these volumes
is recorded in the minutes of the Society’s meeting from 6 June 1705.

![Minutes of the presentation of Lowthorp’s abridgment](JBO-11-113.jpg)

> Mr Lowthorp presented the Society with his Abridgement of the Philosophical Transactions, for which he was Thanked. ([JBO/11](https://catalogues.royalsociety.org/CalmView/Record.aspx?src=CalmView.Catalog&id=JBO%2f11)/113, p. 72)

We can summarize the evidence in this table:

| Lowthorp, 1665–1700                                                                            | Jones, 1700-1720                               | Eames and Martyn, 1719–1733                | Martyn, 1732–44                                            | Martyn, 1743–50                                       |
|:-----------------------------------------------------------------------------------------------|:-----------------------------------------------|:-------------------------------------------|:-----------------------------------------------------------|:------------------------------------------------------|
| Issued as: [v. 1–3](http://estc.bl.uk/T103703) (1705)                                          | [v. 4–5](http://estc.bl.uk/T103708) (1721)     | [v. 6–7](http://estc.bl.uk/T103705) (1734) | [v. 8–9](http://estc.bl.uk/T103701) (1747)                 | [v. 10](http://estc.bl.uk/T103700) (1756)             |
|                                                                                                |                                                |                                            |                                                            |                                                       |
| In 1705: [presented in the minutes](JBO-11-113.jpg) and printed on special paper               |                                                |                                            |                                                            |                                                       |
|                                                                                                |                                                |                                            |                                                            |                                                       |
|                                                                                                | In 1722: [presentation inscription](tp-v4.jpg) |                                            |                                                            |                                                       |
|                                                                                                |                                                | In 1734: presented in the minutes          |                                                            |                                                       |
|                                                                                                |                                                |                                            |                                                            |                                                       |
| [1836 catalogue](https://wellcomecollection.org/works/uqr8bhnq/items?canvas=604): “3 vols.”           | [present]                                      | “4 pts. in 2 vols.”                        | **[absent]**                                                   | [part present]                                        |
|                                                                                                |                                                |                                            |                                                            |                                                       |
|                                                                                                |                                                |                                            | [“Thomas Frederick Colby (1784–1852)”](inscription-v8.jpg) |                                                       |
|                                                                                                |                                                |                                            |                                                            |                                                       |
| [1881 catalogue](https://hdl.handle.net/2027/coo1.ark:/13960/t9b574d6p?urlappend=%3Bseq=75): “3 vols.” | “Vol. 4, 5.”                                   | “Vol. 6, 7.”                               | “Vol. 8 & 9, in 3 vols.”                                   | “Vol. 10”                                             |
|                                                                                                |                                                |                                            |                                                            |                                                       |
|                                                                                                |                                                |                                            |                                                            | [part two arrives]                                    |
|                                                                                                |                                                |                                            |                                                            |                                                       |
| [Form at the Society now](shelf.jpg): v. 1–3                                                   | v. 4–5                                         | v. 6–7                                     | v. 8 pt. 1, v. 8 pt. 2, v. 9                               | v. 10 pt. 1, [v. 10 pt. 2](binding-stamp-v10-pt2.jpg) |
|                                                                                                |                                                |                                            |                                                            |                                                       |
|                                                                                                |                                                |                                            |                                                            |                                                       |
|                                                                                                |                                                |                                            |                                                            |                                                       |


The second part of volume ten looks a bit different than the others.
It has a different style of the arms of the Society stamped to its
spine and it has a binding stamp for [Maltby’s of
Oxford](binding-stamp-v10-pt2.jpg).  The Society used Eyre &
Spottiswood for binding periodicals starting in the late nineteenth
century, but into the twentieth century worked with a wide range of
bookbinders including W. Muller, W. H. Smith, Best & Co., Zaehansdorf,
Taylor & Francis, Dawson, Smith, Crockett, and the University Press at
Oxford.  While I cannot date the last binding exactly, the consistency
of the earlier bindings suggest they were all done together.  Since
the second part of volume ten was not in the library until after 1881,
it seems probable that it was added later.

Thus we know where many of the volumes came from.

* Volumes one through seven, published from 1705 to 1734, were
  presented by their authors to the Society upon their completion.

* Volumes eight and nine must have been owned by [“Thomas Frederick
  Colby (1784–1852)”](inscription-v8.jpg) and purchased by the library
  after his death to complete their collection.

* It the first part of volume ten was present at the end of the
  nineteenth century, and the second part appeared somewhat later.

It’s a little surprising that John Martyn would have presented the
first two volumes of his abridgment work, but not the later ones.
Perhaps when he took up a professorship in Cambridge he did not visit
London as frequently.
