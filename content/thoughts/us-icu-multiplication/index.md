---
title: "Using addition for multiplication"
date: 2022-10-03T08:20:24-05:00
description: 'Sometimes even printers are out of sorts'
image: errata.jpg
dot: "."
---

I’ve been studying the copy of the [*Philosophical
Transactions*](http://pi.lib.uchicago.edu/1001/cat/bib/1018054) at he
University of Chicago Library [Hanna Holborn Gray Special Collections
Research Center](https://www.lib.uchicago.edu/scrc/) on a [Robert L. Platzman Memorial Fellowship](https://www.lib.uchicago.edu/scrc/about/platzmanfellowships/).

The last number of volume two, number 32, has the errata shown at the
top of this webpage that explains that the printer used the wrong mark
to express multiplication in number 29.  Indeed, they used what we
would call a dagger, but which readers seem to have taken as an addition sign.

![Number 29 additional for multiplication](multiplication.jpg)

The printer defends themselves “’tis thought could hardly occasion any
mistake in the Intelligent Readers, who might easily see the meaning
of the Author by the lines 8, 9, 10 of the next precedent page 570.”
(628, 3V4v)  That page seems only mildly less confusing to me.

![Page 570, lines 8-10](p570ll8-10.jpg)

It’s interesting to imagine how a printer thought the second bracketed
line was equivalent to specifying the numbers to multiply separated by
daggers.
