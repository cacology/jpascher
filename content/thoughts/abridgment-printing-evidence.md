---
title: "Printing evidence at the Royal Society"
date: 2022-06-06T12:49:37+01:00
description: 'Describing and interpreting printing evidence at the Royal Society, among it abridgments and orders.'
dot: "."
---

In the mid seventeenth century, Charles II chartered The Royal Society
of London for Improving Natural Knowledge.  Within that charter, he
gave them the right to print texts without using a process layed out
in a previous law, his Licensing of the Press Act of 1662.  The
society’s right became the basis of a vibrant published record
beginning in 1664 that continues to the present day.

The printing they conducted between 1664 and 1695, when the Licensing
of the Press Act official lapsed, differs from the typical accounts of
the history of copyright.  Our received history of the 1662 act claims
that every book was registered with the Stationers’ Company and
approved by an official Licensor.  Registration and licensing were
required by the process in the act of 1662.  However, the act
specifically excludes regulating printing done under the Royal
prerogative, typically monopolies on Bibles and laws, but also the
printing of the society.

Thus the printing conducted under the charter of the society follows
different rules, produces different evidence, and represents a
different legal regime.

This summer, I’m working on assembling that evidence into a series of
articles and talks that help us to understand the other sorts of legal
regimes in which books were printed in early modern London and the
broader anglophone world.  I’ll write more posts as I go along, but
for now, you can see the in-progress essays here:

- [A calendar of printing
  evidence](https://gitlab.com/cacology/rs-printing-evidence)
  
- [An account of a particularly important abridgment](https://gitlab.com/cacology/lowthorp-abridgment)

The work has been generously funded by [Bibliographical
Society](https://bibsoc.org.uk/content/2020-2021-0), the
[Bibliographical Society of
America](https://bibsocamer.org/awards/fellowships/previous-recipients/),
and my own thrift while working at a call center during the pandemic.
