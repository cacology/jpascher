---
title: "Two kinds of correction at US-icu"
date: 2022-09-07T08:20:24-05:00
description: 'Distinguishing corrector’s proofs from reader’s corrections in the Transactions'
image: del-no81.jpg
dot: "."
---

I’ve been studying the copy of the [*Philosophical
Transactions*](http://pi.lib.uchicago.edu/1001/cat/bib/1018054) at he
University of Chicago Library [Hanna Holborn Gray Special Collections
Research Center](https://www.lib.uchicago.edu/scrc/) on a [Robert L. Platzman Memorial Fellowship](https://www.lib.uchicago.edu/scrc/about/platzmanfellowships/).

I’m establishing the basic facts of the printing history to link up to
the legal and editorial history of Henry Oldenburg’s work on the
*Transactions* from 1665 to 1677.

Today, I spotted a rare survival. Look at the correction above on this
webpage from number 81 for 25 March 1672.  Compare it with this one
from number 82 for 22 April 1672.

![Insertion in number 82](insert-no82.jpg)

Someone reading the text after it was bound into a volume made one
correction.  Someone working at the printer’s shop made the other
correction.  Reader’s corrections can be seen fairly frequently in
seventeenth-century books like this one.  Fewer than a hundred
surviving examples of printer’s proof correction from the seventeenth
century have been recorded.

In other words, one is neat, but not all that unusual.  The other is a
rarely recorded survival of primary source evidence from a
seventeenth-century printing shop. (**Side note:** *I don’t actually think these are
as rare as they appear to be.  I think there are many more, like this
one, lurking in books.*)

## Look again, think about it.

## .

## .

## .

## Okay, want a hint?

## .

## ‌      .

## ‌             .

[Here’s a hint.](https://www.biodiversitylibrary.org/page/46836681)

## ‌             .

## ‌      .

## .

[Here’s another one.](https://www.biodiversitylibrary.org/page/46836731)

## .

## ‌             .

## .

[Here’s your penultimate hint](pp4018-4019-no81.jpg) and your [ultimate hint](p4051-no82.jpg).

## ‌             .

## .

## ‌             .


Okay, tell me what you think!  Which is which?  Maybe you see
something I don’t?

Email me or message me below and I’ll post your answer if you want.
