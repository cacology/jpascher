---
title: "More volumes of the Transactions from the Calvary bookshop"
date: 2022-09-26T08:20:24-05:00
description: 'The Calvary bookshop sent a first batch of Transactions and then a second'
image: log-entry.jpg
dot: "."
---

I’ve been puzzling through [the copies of the Transactions](/thoughts/us-icu-pt-circulating) at he University of
Chicago Library [Hanna Holborn Gray Special Collections Research
Center](https://www.lib.uchicago.edu/scrc/) and in the [Joe and Rika
Mansueto Library](https://www.lib.uchicago.edu/mansueto/).

The new university President bought the [S. Calvary and Company’s
entire book collection to establish the
library.](/thoughts/us-icu-lowthorp-calvary) Hugo Bloch at
K.F. Koehler’s Antiquarium acted as an agent for the university.

Above you can see the manuscript catalog entry for the *Philosophical
Transactions* that Bloch wrote for the periodicals he sent to Chicago
from Berlin.  (source: [University of Chicago. Library. Office of the
Director. Zella Allen Dixson. Records, Box 5, Folder 10, Hanna Holborn
Gray Special Collections Research Center, University of Chicago
Library](https://www.lib.uchicago.edu/e/scrc/findingaids/view.php?eadid=ICU.SPCL.LIBRARY1))
The catalog arrived with the first batch of volumes, [many now in the
research collection.](/thoughts/us-icu-pt-circulating) A second batch
arrived 23 Nov 1893, as the the Calvary bookshop tried to fulfill
their promise of [completing the periodical
sets.](/thoughts/us-icu-lowthorp-calvary) The volumes in the second
batch ended up split between two present-day collections.

The earlier volumes are in the [Hanna Holborn Gray Special Collections Research
Center](https://www.lib.uchicago.edu/scrc/).

![Early volumes on a cart](spc-cart.jpg)

The later volumes are in the [Joe and Rika
Mansueto Library](https://www.lib.uchicago.edu/mansueto/).

![Later volumes on a cart](circulating-cart.jpg)

Unlike the volumes in the [first batch from the Calvary bookshop](/thoughts/us-icu-lowthorp-volumes), these seem to have come from several sources so lack the penciled numbers on the title pages and other distinguishing marks.  If we put these volumes in a table with their evidence [like we did the previous batch’s volumes](/thoughts/us-icu-pt-circulating) some patterns emerge.

| Vol. 97 (1807)                                                             | 99 (1809)                                                                   | 100 (1810) | 102 (1812) | 104 (1814)                        | 105 (1815) |
|:---------------------------------------------------------------------------|:----------------------------------------------------------------------------|:-----------|:-----------|:----------------------------------|:-----------|
| Binding: [Modern dark blue cloth (mdbc)](binding-v97.jpg)                  | mdbc                                                                        | mdbc       | mdbc       | mdbc                              | mdbc       |
|                                                                            |                                                                             |            |            |                                   |            |
| On spine, gilt: [rule, title, volume, date, rule (rtvdr)](binding-v97.jpg) | rtvdr                                                                       | rtvdr      | rtvdr      | rtvdr                             | rtvdr      |
|                                                                            |                                                                             |            |            |                                   |            |
| Binding stamp: [Newberry 8/64 (N)](stamp-v97.jpg)                          | N                                                                           | N          | N          | N                                 | N          |
|                                                                            |                                                                             |            |            |                                   |            |
| Bookplate: [institution white-line (iw)](bookplate-v97.jpg)                | iw                                                                          | iw         | iw         | iw                                | iw         |
|                                                                            |                                                                             |            |            |                                   |            |
| Accession: 198034                                                          | 98008                                                                       | 198037     | 198039     | 198041                            | 198042     |
|                                                                            |                                                                             |            |            |                                   |            |
|                                                                            | On t.p: [Calvary (ink, upper left), 1897 (pencil, lower right)](tp-v97.jpg) |            |            |                                   |            |
|                                                                            |                                                                             |            |            |                                   |            |
|                                                                            | On t.p.: ‘1897’ (penciled, lower right)                                     |            |            | On t.p + 2: 25954 (pencil bottom) |            |
|                                                                            |                                                                             |            |            |                                   |            |

----

| Vol. 106 (1816)   | 107 (1817) | 170 (1879)                                                  | 171 (1880)                                                             | 172 (1881)                                                                                                        | 173 (1882)                                                        |
|:------------------|:-----------|:------------------------------------------------------------|:-----------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------|:------------------------------------------------------------------|
| Binding: mdbc     | mdbc       | [modern strong blue binding](binding-v170.jpg)              | boxed, [modern black cloth (mbc)](binding-v171.jpg),                   | b, [strong yellowish brown contemporary paneled calf](upper-binding-v172.jpg) with [modern spine](spine-v172.jpg) | [modern deep blue cloth (mdebc)](spine-v173.jpg)                  |
|                   |            |                                                             | [rebound 31 Jan – 11 April, 1956](card-v171.jpg)                       |                                                                                                                   |                                                                   |
|                   |            |                                                             |                                                                        |                                                                                                                   |                                                                   |
| On spine: rtvdr   | rtvdr      | [title, volume, year, call number (tvyc)](binding-v170.jpg) | tvyc                                                                   | [lettering piece with title and year, volume, call number](spine-v172.jpg)                                        | [tvyc](spine-v173.jpg)                                            |
|                   |            |                                                             |                                                                        |                                                                                                                   |                                                                   |
| Binding stamp: N  | N          |                                                             |                                                                        |                                                                                                                   |                                                                   |
|                   |            |                                                             |                                                                        |                                                                                                                   |                                                                   |
| Bookplate: iw     | iw         | iw                                                          | [Berlin Collection (BC)](bookplate-v171.jpg)                           | [institutional](bookplate-v172.jpg)                                                                               | BC                                                                |
|                   |            |                                                             |                                                                        |                                                                                                                   |                                                                   |
| Accession: 198043 | 198044     | 98026                                                       | 98027                                                                  | 198109                                                                                                            | 98028                                                             |
|                   |            |                                                             |                                                                        |                                                                                                                   |                                                                   |
|                   |            |                                                             | On t.p.: [‘1897 \| 384’ (pencil, lower right)](lowerright-tp-v171.jpg) |                                                                                                                   | ‘1897 \| 384’ (pencil, lower right)                               |
|                   |            |                                                             |                                                                        |                                                                                                                   |                                                                   |
|                   |            | On t.p. + 2: 1923 (pencil, bottom)                          | ‘349210’ (ballpoint, bottom)                                           | ‘165199’ (stamped)                                                                                                | ‘71397’ (stamped, light greenish blue), ‘339986’ (stamped, black) |
|                   |            |                                                             |                                                                        |                                                                                                                   |                                                                   |

----

| Vol. 174 (1883)                                                                           | 175 (1884)                                            | 176 (1885)                                                 | 177 (1886)                              | 178 (1887)                                  |
|:------------------------------------------------------------------------------------------|:------------------------------------------------------|:-----------------------------------------------------------|:----------------------------------------|:--------------------------------------------|
| Binding: [modern blackish blue cloth (mbbc)](spine-v174.jpg)                              | [modern black binding (mbb)](spine-v175.jpg)          | [modern moderate bluish green cloth](spine-v176.jpg)       | mbc                                     | mdebc                                       |
|                                                                                           | rebound 15 Jan 1948                                   | 16 Aug 1964                                                |                                         | 4 – 8 Aug 1954, 9 Oct 1954, 5–28 April 1955 |
| On spine: [five panels: blank, title, volume, year, call number (5btvyc)](spine-v174.jpg) | rule, title, volume, year, call number, rule (rtvycr) | title, volume, year, call number [[drawn]](spine-v176.jpg) | rtvycr                                  | tvyc                                        |
|                                                                                           |                                                       |                                                            |                                         |                                             |
| Bookplate: BC                                                                             | BC                                                    | iw                                                         | institutional                           | BC                                          |
|                                                                                           |                                                       |                                                            |                                         |                                             |
| Accession: 98029                                                                          | 98030                                                 | 98031                                                      | 98032                                   | 98033                                       |
|                                                                                           |                                                       |                                                            |                                         |                                             |
| On t.p.: ‘1897 \| 384’ (pencil, lower right)                                              |                                                       | ‘1897 \| 384’ (penciled, lower right)                      | ‘1897 \| 384’ (penciled, lower right)   | ‘1897 \| 384’                               |
|                                                                                           |                                                       |                                                            |                                         |                                             |
| On t.p. + 2: ‘168460’ (stamped, light greenish blue)                                      | ‘174298’ (stamped, light greenish blue)               |                                                            | ‘235531’ (stamped, light greenish blue) | ’332007’ (ballpoint, bottom)                |
|                                                                                           |                                                       |                                                            |                                         |                                             |

----

| Vol. 179 A (1888)                                   | 179 B (1888)                                                                                             | 180 (1889)                                                                          | 181 A–B (1890)                               |
|:----------------------------------------------------|:---------------------------------------------------------------------------------------------------------|:------------------------------------------------------------------------------------|:---------------------------------------------|
| Binding: mbbc                                       | [modern strong brown cloth](spine-v179b.jpg)                                                             | [boxed, repaired 19c Ger. binding typical of previous batch (19cG)](boxed-v180.jpg) | [19cG](boxed-v181ab.jpg)                     |
|                                                     |                                                                                                          |                                                                                     |                                              |
| On spine: 5btvyc                                    | [Royal Society, title, series, volume, year, Crerar library](spine-v179b.jpg)                            | [heavily repaired with tape (hrt)](boxed-v180.jpg)                                  | [hrt](boxed-v181ab.jpg)                      |
|                                                     |                                                                                                          |                                                                                     |                                              |
| Bookplate: BC                                       | [Crerar](bookplate-v179b.jpg)                                                                            | BC                                                                                  | [BC with Dewey number](bookplate-v181ab.jpg) |
|                                                     |                                                                                                          |                                                                                     |                                              |
| Accession: 98034                                    |                                                                                                          | 340907                                                                              | 98035                                        |
|                                                     |                                                                                                          |                                                                                     |                                              |
| On t.p.: ‘1897 \| 384’ (penciled, lower right)      | On t.p.: ‘L062.76 \| 17’ (penciled, bottom middle), ‘173607’ (stamped, dark greenish blue) over ‘27/353’ |                                                                                     | ‘1897 \| 384’ (penciled, lower right)        |
|                                                     |                                                                                                          |                                                                                     |                                              |
| On t.p. + 2: ‘151739’ (stamped, dark greenish blue) |                                                                                                          | ‘69061’ (stamped, light greenish blue)                                              |                                              |

----

The accession numbers beginning with 198034 were purchased from
another source and added to the [accession record in 1904.](accessions-v20-198001-40.jpg)
Consistently, none of them have the bookplates, pencil markings, or
bindings we’ve seen on books from the Calvary bookshop.

The “Newberry” marking apparently indicates that these volumes had
been transferred to the Newberry Library and back again, possibly
merely to be rebound.  The only volume that seems to have been part of
the second Calvary batch that bears that marking is volume 99
from 1809.  It’s binding, however, matches the other volumes that were
transferred to the Newberry and back.  Note that the penciled notation
typical of the Calvary copies, “1897 \| 384”, is cropped in the
Newberry copy.

The presence of a Berlin Collection bookplate seems to consistently
indicate items from the Calvary bookshop, but the absence of such a
bookplate does not demonstrate that the book was not from that shop.

Volume 172 seems to be from a wholly different source.  It’s binding
is unlike any other and its accession number is outside of the other
ranges.  Volume 179, series B, comes from the [Crerar
Library](https://www.lib.uchicago.edu/crerar/about-crerar-library/history/)
so was added to the university’s collection after accession numbers
had ceased to be used.

The cloth used for rebinding in the 1940s to the 1960s seems to have
been anywhere between moderate bluish green to blackish blue or black.
Since the records of rebinding come from the library cards at the
back, there may be unrecorded rebinding and it’s possible that a
variety of cloths were used in each era.  The letterforms stamped on
the spine are also wildly inconsistent between bindings.  Perhaps a
variety of binderies were contracted and each had their own style?

The volumes, however, clearly come from a variety of sources, not just
the Calvary bookshop.  While the accession books record these books
and do not record them being withdrawn, this does not demonstrate that
they are from the Calvary bookshop.  It seems probable that as new
collections were added, the librarians replaced worn-out copies with
those in better shape.

The current run of the *Transactions*, thus, outlines the history of
the library’s acquisition of materials.  I propose this hypothesis:

> The history of a library’s copies of the *Philosophical Transactions* coincides with the history of the library.
