---
title: "Editing for submerged voices"
date: 2021-05-25T19:47:27+01:00
description: "Using archival and printed evidence to recover submerged voices within our historical record."
dot : "."
---

When we link archival evidence to printed evidence, we can recover all
sorts of things that we missed on the first pass.

Looking at a report of a lump of maple sugar in the *Philosophical
Transactions* of the Royal Society of London, I argue we can actually
know a great deal more about the contributions of the people
indigenous to North American than we think we can.

{{< youtube id="H8lqcsxkwCw?start=1281" >}}

[https://youtu.be/H8lqcsxkwCw?t=1281](https://youtu.be/H8lqcsxkwCw?t=1281) (appx. 21:22 - 34)

I use a digital forensic model to make an argument about time not
being as simple as we imagine, and that messy chronologies can show us
people that were involved, that we forgot about.

I'm thinking about archival time.  How do we misunderstand time
presented in archives and printed books?  How can we begin to recover
how it was really working?  What else do we know by remembering all
the stops along the way?

You can see a [written version of the talk I gave at MLA at this post.](/thoughts/indigenous-peoples-contributions/)
