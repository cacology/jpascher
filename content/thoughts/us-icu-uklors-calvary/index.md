---
title: "A Royal Society book at US-icu"
date: 2022-09-21T08:20:24-05:00
description: 'Spreading knowledge by selling books'
image: stamp.jpg
dot: "."
---

I’ve been studying the copy of the [*Philosophical
Transactions*](http://pi.lib.uchicago.edu/1001/cat/bib/1018054) at he
University of Chicago Library [Hanna Holborn Gray Special Collections
Research Center](https://www.lib.uchicago.edu/scrc/) on a [Robert L. Platzman Memorial Fellowship](https://www.lib.uchicago.edu/scrc/about/platzmanfellowships/).

As part of that work I’ve been trying to understand the sources of
their *Transactions*, which include the [Calvary bookshop of
Berlin](/thoughts/us-icu-pt-calvary-batches).  The volumes of the
*Transactions* from Calvary have a distinctive binding in half,
morocco-grained sheep with a gold stamp for the library in the lower
left of the upper board on the sheep.  The binding on [this Greek
grammar](https://catalog.lib.uchicago.edu/vufind/Record/1201288)
follows the same pattern, but on green cloth.

![Binding for PA253](binding.jpg)

It has the [bookplate](plate.jpg) and markings associated with the
Berlin collection purchased by the university in the late 19th
century.

But at the top of this webpage you can see a stamp on the verso of the
title page marking this book as the property of the Royal Society of
London, given by Henry Howard as part of his Norfolk library.  In the
[catalog of Henry Norfolk’s donation](http://estc.bl.uk/R14407), this
book is listed in the *G* section as “Gretseri (Jac.) Grammatica
Græca.” (57)  At the end of the book is a stamp that explains why this book is in Chicago.

![Sold stamp](stamp-sold.jpg)

[Linda Peck](https://doi.org/10.1098/rsnr.1998.0031) explains that the
Royal Society sold books three times.  In 1829 they sold manuscripts
to the British Museum.  From 1924 to 1925, they auctioned books at
Sotheby’s.  And in the 1870s, they sold books through the bookseller
Bernard Quaritch who listed many of them in their 1873 catalogue.

Since this book was purchased from the Calvary bookshop in late
nineteenth century, it seems most likely that it was part of the
Quaritch sale.  The topic aligns with the interests of the Calvary
bookshop, which specialized in philology and sold the university an
extensive selection of ancient Greek authors.

## The Improvement of Knowledge through the Circulation of Books

American libraries purchased many early books like this one from the
Royal Society sales, transferring the books from one continent to
another.  This transfer makes perfect sense with the mission of the
Royal Society and the aims of American institutions at the time.  The
Royal Society still sought to spread knowledge, but found this books
outside of its active interests.  American institutions sought to find
historical materials to build their collections, and had newly
developed interests.

A great deal of useful research material can be found because of the
thoughtful dispersal of unwanted books.  This one was in the
University of Chicago’s founding philological collection.
