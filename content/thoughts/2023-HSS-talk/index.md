---
title: "2023 HSS Talk"
date: 2023-11-06
description: 'Paperwork history as banking and its relationship to printing.'
private: true
dot: "."
---

## Disseminating Knowledge with Commerce: the Banking Model of Knowledge at the Royal Society of London during the Late Seventeenth Century

[Slides in Keynote, 32.1 MB](Slides.key)

[Slides in PDF, 11.5 MB](Slides.pdf)

### Accessibility Outline of talk

[Title slide:]

The Royal Society of London in the late seventeenth century maintained
correspondence with the curious globally.  Managed by one of their
earliest Secretaries, Henry Oldenburg, the incoming and outgoing
letters disseminated knowledge across the European experience of the
world.

That these letters were used to print both a journal and many books is
well-known.  What is comparatively less studied is the paperwork
history involved in managing these materials.

My talk presents some of my preliminary work into understanding the
paperwork history of Oldenburg’s office, one of the largest European
offices of the seventeenth century.  In particular, I argue that
Oldenburg’s model of docketing, copying, checking, printing, selling,
and accounting for discrete contributions developed by combining
established models for banking and those used by a working
intelligencer.

I’m grateful to the Linda Hall Library, the Bibliographical Society of
London, the Bibliographical Society of America, and the Lisa Jardine
Grant Scheme of the Royal Society for supporting this research.  Like
the Society did in the seventeenth century, these farsighted scholarly
organizations have generously given me and many others the time to
work through some difficult problems.

[Next slide:]

This letter from Johannes Hevelius to Henry Oldenburg discusses some
Kepler manuscripts, some innovations in astronomy, and a new book by
Matthias Wasmuth.  By 1674, Oldenburg had become fairly established as
a correspondent for such kinds of news, so Hevelius would not have
been surprised that some of his letter was published in the
*Transactions* and sold.

[PT slide:]

It’s worth noting we already have text as sort of commodity here:
Hevelius reports on texts whose value is attested to--at least in
part--as currency “by Kepler” and the currency of the Wasmuth book
relates directly to whether or not someone might buy a copy, or print
another edition.

But the Philosophical Transactions here is also a commodity selling
up-to-date philosophical news from London.  It reprints extracts from
his letter, describing the Kepler manuscripts and the astronomy.  This
issued number itself was sold and informed the growing interest in
knowledge circulating as a commodity.

[Extract slide:]

The letter has a number of features showing us how it was transformed
into this printed and sold book.  At the head is a brief outline of
its argument written by Oldenburg that mirrors the same argument at
the head of the printed version.  The text itself is taken from the
letter, but edited down.

[More features slide:]

This letter was copied rather than printed directly.  But even with
that, it contains a number of marks documenting the process.

Beyond the argument at the head written by Oldenburg, we have this
note that it was entered into the seventh volume of the letter books
starting on page 77 and we have a little cross in the lower corner
from a later reconciliation of the letters owned in the archive and
listed in their accounts.

The orange lines are folds from when this was mailed in a letter.

[File folded slide:]

However, we have a second set of folds from when this letter was filed.

[Close up file folded slide:]

You can see that the docketed argument comes from the era of filing
because it fits within this particular folding scheme.  Similarly, the
note about entry in the letter books comes from this filing era.

[Next letter slide:]

Other letters bear some of the same marks since they were accounted
for similarly.  This letter has a few more kinds of marks.

[Zoom in of letter:]

Here we have Oldenburg’s argument at the foot of the letter, docketing
the accounting for both being entered and read, and crochets in the
text marking out certain passages.

[Picture of letter with printed page:]

These crochets and the argument facilitate the printing in the
Transactions and thus the sale of the contents and intelligence in the
letter.  But I want to emphasize that these marks also deal with the
internal management and accounting of correspondence.  This next
letter has even more.

[Letter with twice filed:]

This is another letter from Hevelius.  At this point, you can see the
crochet on the left and the argument on the right, along with some of
the folds.

[Highlighted folds:]

But this letter was never printed and the folds and their accompanying
docketing indicate different archival regimes.  The top docketing
involves the letter being folded in half while postal folded and
accounts for Oldenburg’s response.  He must have had a box of incoming
letters to which he needed to respond and he marked the response on
the letter itself.

The other marking is Oldenburg’s argument, still on the letter folded
form, but now in a location away from the response.  It must have been
the result of a filing system where Oldenburg kept track of the
various facts and ideas.  Since this was never printed, this argument
must have been part of his internal system of accounting for the ideas
that he had received.

Additionally, we can see the quarter folds on this too, which indicate
a third filing regime for this, but which don’t have any new docketing.

[Wallis letter:]

This hardly exhausts the kinds of marks that indicate both internal
use and printing, but I think demonstrates the point I’m trying to
make: Oldenburg manages the incoming and outgoing correspondence as a
sort of resource that he can account for and draw material from.  Far
from a series of letters, these are more akin to the goods kept by a
grocer or a merchant.  He accounts for their arrival, their
transformation, and their dissemination in various forms.

[Chart:]

The work of the Society aims to improve knowledge and they record
these letters alongside their finances.  The journal books account
just as much for dues, fees, and donations as new ideas submitted in
documents.

This I think is the key innovation.

This chart outlines the document paths and the evidence they produce.
What I want you to notice here is that we have a series of journals,
registers, and minutes accounting for the transformation of one kind
of intelligence to another.  Each archival shift happens in the
process of reconciling some account with another and the Society
treats these letters as bearers of valuable knowledge.

I color the lines to indicate the kind of evidence each step of the
process leaves.  The dotted lines mark the paths recording takes: that
is, an account in a Journal book or register, or some such collection
of business minutes.

Within the “Filed” category are a variety of activities I don’t have
time to get into today, but which show the archive being adjusted,
moved, and managed by different people and groups toward different
aims.

[Council minutes:]

Indeed, after Oldenburg dies, the Council writes up formal procedures
that reproduce his paperwork methods.  I think this strongly suggests
that this is where the office innovation lies: as a former
intelligencer tasked with managing one of the first learned Societies,
Oldenburg adapts techniques from banking to build a more complete
office than early-modern England had imagined possible for mere
knowledge.

This knowledge was adapted into a commercial-like object so that it
could be accounted for and in this accounting it was able to be
disseminated in new channels that previously excluded written
knowledge.

[Talk about the minutes a little bit, if you have time, and show the
chart again.]
