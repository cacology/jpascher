---
title: "Sources of the Philosophical Transactions at US-icu"
date: 2022-10-10T08:20:24-05:00
description: 'The Transactions as a emblem of cooperation and sharing'
image: PT-copies.png
dot: "."
---

I had been studying the copy of the [*Philosophical
Transactions*](http://pi.lib.uchicago.edu/1001/cat/bib/1018054) at he
University of Chicago Library [Hanna Holborn Gray Special Collections
Research Center](https://www.lib.uchicago.edu/scrc/) on a [Robert
L. Platzman Memorial
Fellowship](https://www.lib.uchicago.edu/scrc/about/platzmanfellowships/).

During my fellowship, I saw an excellent talk at the Linda Hall
Library.  [Aileen
Fyfe](https://www.st-andrews.ac.uk/history/people/akf) described her
ongoing work studying how the Royal Society distributed copies of the
*Transactions* and [Jamie Cumby](https://twitter.com/JECumby)
described the Linda Hall’s copies of the *Transactions*.  It’s [worth
watching in
full.](https://afterhoursphilosophicaltransactions.splashthat.com)  The book on [which it is based is available now via open access.](https://www.uclpress.co.uk/collections/contact-137825/products/187262)

I’ve been looking at lots of copies of the *Transactions* acquired
piecemeal from various sources, long after their production.  The talk
made me wonder what it would look like to try to understand the
sources of a run of *Transactions* that was assembled as a reference
resource, but long after many volumes had been printed.

In previous posts, I described [tracing the
sources](/thoughts/us-icu-pt-calvary-batches/) of the various volumes
of the *Transactions*.  I hypothesized that the history of a library’s
copies of the *Philosophical Transactions* coincides with the history
of the library.  To test that hypothesis too, I traced more volumes
and prepared the visualization above on this website.

The visualization puts the volumes of the multi-century *Transactions*
into twenty-five year sections.  The left of the visualization occurs
in 1665 the right in 2025.  The events and sources are placed roughly
in the quarter century in which they belong.

The white space to the left can make the image hard to read, but
represents visually the amount of time that passed before the
University of Chicago existed to begin acquiring these old books.
Here’s a [full-sized version as PNG](PT-copies.png) and a [PDF version
that is easier to print.](PT-copies.pdf)

Today, the volumes of the *Transaction* can be found split between the
rare book collection and the on-site high-density storage in the [Joe
and Rika Mansueto Library](https://www.lib.uchicago.edu/mansueto/).
But these volumes come from a variety of sources.

## Calvary bookshop

The first acquisition comes via the Calvary bookshop and I’ve [written about those elsewhere](/thoughts/us-icu-pt-calvary-batches) and [described their typical appearance.](/thoughts/us-icu-lowthorp-calvary)

The only new evidence that looking at all these volumes uncovered is a binding ticket in volume 161 that otherwise looks like the typical Calvary bookshop binding.

![Chicago Job Book Bindery ticket](bindery-ticket.jpg)

While the volume looks quite similar to other Calvary volumes, it also
has repaired cloth hinges, lighter marbled paper, and lacks the stamp
for the University of Chicago that others have.  It’s possible the the
Chicago Job Book Bindery repaired this book—no other book in the two
hundred or so I’ve looked at has a similar ticket.  It’s also possible
that the Chicago Job Book Bindery bound this one to look like those
volumes.  Two other volumes, number 150 and 148, have the lighter
marbled paper like this one and their spines are exposed.  They’re
lined with brown paper, not Berlin-based newspapers.

## Brasenose College

The second acquisition comes from Brasenose College, Oxford.  Volume
11 (1676) has ‘Liber Aula Regiæ \| & Collegij de Braſenoſe.’ on the
page facing the title page and volume 169 (1878) has an ownership
stamp.

![Brasenose ownership stamp](brasenose-stamp.jpg)

The Calvary accession has been combined with the Brasenose accession, but some volumes in the on-site storage have their original binding still.  The upper boards are paneled calf, mostly detached like this one from volume 133:

![Vol 133 upper board](v133-upper-board.jpg)

Their [spines](v133-spine.jpg) have matching lettering pieces and volume 133 has an original Dewey card with no evidence of circulation between 1902 and the present day.

![Vol 133 circulation card](v133-card.jpg)

Volume 125 has the bookplate typical of this accession and what must be the shelf mark from Brasenose.  Volume 113 has a similar shelf mark [reading “A.26.19” instead.](v113-plate-mark.jpg)

![Vol 125 plate and mark](v125-plate-mark.jpg)

The letter possibly refers to the shelving bay, the second number the
shelf, and the third number the volume on a particular shelf.

## John Crerar Library

The [John Crerar Library](https://www.lib.uchicago.edu/crerar/) is now
located on the University of Chicago campus, but had been at several
other locations.  The first librarian, Clement Walker Andrews,
[established subscriptions for current materials and sought older,
rare
titles.](http://storage.lib.uchicago.edu/pres/2011/pres2011-0096.pdf)
The volumes that bear marks from the Crerar library, or have digital
records recording their source, begin slightly before the foundation
of that library and continue to the present day.  A typical binding is
like the one on volume 179B that names the library right on the spine.

![Volume 179B spine](v179-spine.jpg)

Volume 179B also has a [bookplate indicating its
source](v179b-plate.jpg), [perforation for the
library](v179B-perforation.jpg), and a [penciled Dewey call
number](v179B-mark.jpg) prefixed with the letter *L*.

After the Crerar Library merged with the University of Chicago in
1984, that library continued to acquire the new volumes of the
*Transactions*.  The copies acquired today are still purchased for and
by the John Crerar Library, but stored in the on-site storage facility
rather than a separate collection.

## Rebinding

Herman H. Fussler’s 1964-65 annual report for the University of
Chicago Libraries explains that “[s]ince the early days of the
University, the great bulk of the Library’s binding has been handled
at the University Press” but that during that year they had been using
“outside commercial binderies” to deal with increasing needs.
Furthermore, “a major effort has been devoted to the improved physical
care of rare books.” (p. 6 of [Fussler, Herman Howe. Papers, Box 17,
Folder 1, Hanna Holborn Gray Special Collections Research Center,
University of Chicago
Library.](https://www.lib.uchicago.edu/e/scrc/findingaids/view.php?eadid=ICU.SPCL.FUSSLERHH))

The copies now held in the rare book collection have a uniform binding
that dates from 1964, which must have been one of the early cases of
improving the care of rare books.  The volumes come from two sources,
but the bindings all look the same.  None of the Crerar volumes have
this binding because of the timing of that conveyance.

![Newberry bindings](/thoughts/us-icu-pt-calvary-batches/spc-cart.jpg)

The bindings have a stamp that suggests that these were bound at the Newberry library as a set August, 1964.

![Vol 105 stamp](v105-stamp.jpg)

Although the bindings no longer distinguish the copies, the [accession numbers can be used](us-icu-lowthorp-accession) or other marks.  For the Brasenose accession, there is often browning where the original leather was in contact with the preliminary pages.

![Vol 105 browning](v105-brown-perforation.jpg)

## On-site storage

The vast majority of volumes can now be found in the high-density
storage that’s part of the [Joe and Rika Mansueto
Library](https://www.lib.uchicago.edu/mansueto/).  Some of the
evidence survives only in these volumes. 

![Storage](storage.jpg)

## Conclusion

This sketch has been necessarily brief, but I think supports my
hypothesis.  To understand the volumes of the *Transactions* available
at the University of Chicago, is to understand not only the history of
the institution but also the history of libraries in Chicago and how
they cooperated with each other.  The physical form of the
*Transactions* improved knowledge by encouraging cooperation between
longstanding Chicago institutions themselves aiming to improve
knowledge.
