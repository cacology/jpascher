---
title: "The accessions at US-icu in 1892"
date: 2022-09-05T08:20:24-05:00
description: 'The accession logbook for Lowthorp’s abridgment uncovers a number of mysteries all at once'
image: nov-2-1892.jpg
dot: "."
---

This series of thoughts has been puzzling out John Lowthorp’s [abridgment
of the *Philosophical Transactions of the Royal Society of
London*](/thoughts/us-icu-lowthorp) at he University of Chicago
Library [Hanna Holborn Gray Special Collections Research
Center](https://www.lib.uchicago.edu/scrc/).  We last [noted that the
series of works had similar
bindings](/thoughts/us-icu-lowthorp-volumes) and we had already noted some sequential numbers.

[Dr Rebecca
Flore](https://www.lib.uchicago.edu/about/directory/staff/rebecca-flore/)
recognized these as accession numbers from the library accession
records and sent the picture above. (source: [University of
Chicago. Library. Office of the Director. Zella Allen Dixson. Records,
Volume 10, Hanna Holborn Gray Special Collections Research Center,
University of Chicago
Library](https://www.lib.uchicago.edu/e/scrc/findingaids/view.php?eadid=ICU.SPCL.LIBRARY1))
In it you can see the source of the numbers: these are accession
numbers that were assigned in sequential order on 2 November 1892.  In
the left most column, you can see the sequential numbers.  The first
volume has ‘99931,’ the next ‘99932,’ and so forth.  If you look
further down the list, the volume from 1836 ought to have the number
‘99956.’  Indeed, it does on the upper center of the first recto,
right hand, page after the title page.

![Volume 126](numbers-v-126.jpg)

While the volume has been rebound in library buckram, it has the same hair vein marbled edges as the others do.

![Edges of volume 126](edge-v-126.jpg)

Even better, it still has the check-out card in the back, which explains the brown pocket on the preceding volumes and explains the stamped number as well.  It is another inventory number used at the same time as the written one.  Both the first record and the card have the same color stamped number ‘217481.’

![Check-out cards of volume 126](checkout-v-126.jpg)

We can now establish a little history of these volumes: in 1892 the
abridgment volumes were put on the shelf just before the later
nineteenth century volumes.  As the university was new at this point,
these abridgments must have been meant to cover the first years of the
*Philosophical Transactions*.  As time went on, the volumes were
reclassified and got an additional set of numbers and—eventually—some
of them stayed in the general collections whereas some were stored in
the rare book collection.  To summarize,

| John Lowthorp, 1665-1700                                         | Henry Jones, 1700-1720                                           | J. Eames and J. Martyn, 1719–1733                                                | Unabridged, 1803–6, 1811, 1818–20, 1824–26, 1826, 1827–35 | Unabridged, 1836                                                 |
|:-----------------------------------------------------------------|:-----------------------------------------------------------------|----------------------------------------------------------------------------------|-----------------------------------------------------------|:-----------------------------------------------------------------|
| Printed London, 1705,                                            | Printed London, 1721,                                            | Printed London, 1743,                                                            |                                                           | Printed London, 1836,                                            |
| as volumes 1–3:                                                  | as volumes 4–5:                                                  | as volumes 6–7:                                                                  |                                                           | as volume 126:                                                   |
|                                                                  |                                                                  |                                                                                  |                                                           |                                                                  |
| [Q41.L74 v.1]((http://pi.lib.uchicago.edu/1001/cat/bib/2445473)) | [Q41.L74 v.5]((http://pi.lib.uchicago.edu/1001/cat/bib/2445473)) | [Q41.L74 v.6–7]((http://pi.lib.uchicago.edu/1001/cat/bib/2445473))               |                                                           | [Q41.L72 v.126](http://pi.lib.uchicago.edu/1001/cat/bib/1015306) |
| Accession number: 99931                                          | 99932                                                            | 99933–4                                                                          | 99935–99955                                               | 99956                                                            |
|                                                                  |                                                                  |                                                                                  |                                                           |                                                                  |
| Old Dewey number: 062.R81 v.1                                    | 062.R81 v.5                                                      | 062.R81 v.6–7                                                                    | 062.R81 1803–35                                           | 062.R81 1836                                                     |
|                                                                  |                                                                  |                                                                                  |                                                           |                                                                  |
| 19c German, half morocco grained institutional sheep (repaired)  | 19c German, half morocco grained institutional sheep             | 19c German, half morocco grained institutional sheep (v.6); modern buckram (v.7) |                                                           | modern buckram                                                   |
| hair vein marbled edges                                          | hair vein marbled edges                                          | hair vein marbled edges (both vols.)                                                          |                                                           | hair vein marbled edges                                          |

The remaining mystery is how these volumes could have been both German-made and for the University of Chicago.  The evidence that cinches it is in the accessions above, [“Calvary purchase.”](/thoughts/us-icu-lowthorp-calvary)
