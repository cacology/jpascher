---
title: "Guide numbers on the plates of Lowthorp’s abridgment"
date: 2022-08-23T08:20:24-05:00
description: 'Guide letters on an illustrated plate explain the history of the engraving'
image: guide-numbers.jpg
dot: "."
---

The University of Chicago Library [Hanna Holborn Gray Special
Collections Research Center](https://www.lib.uchicago.edu/scrc/)[copy
of Lowthorp’s abridgment](/thoughts/us-icu-lowthorp) of the
*Philosophical Transactions of the Royal Society of London* has
lightly written numbers by many figures on the illustrated plates.  In
most cases, they are reversed as they are in the image above.

These numbers would have been written forward on the engraved printing
plate, so they would have printed backwards.  Some numbers, however,
are forward reading now which means they were written backwards on the
printing plate.

These illustrations reproduce dozens of figures per plate from the
*Transactions*.  John Lowthorp renumbered the illustrations in a
different order, so these numbers were written by the engraver to
guide the work for one of two reasons that I can think of.

I think that either they laid out the plate in advance or had a
separate person to engrave the numbers.  In the first case, the
engraving team would have written a small number where the figure
would go.  Since the plates packed in [dozens of
figures](whole-plate.jpg), laying them out would have made sure to fit
them all in together.

On the other hand, it might be that whoever cut the figure numbers did
not cut the original images.  Many of the geometric figures also use
guide lines to achieve accuracy, so it might have been that two
specialized engravers were involved.  One engraver copied good
geometric diagrams; one engraver cut the numbers onto the figures.

