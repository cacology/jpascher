---
title: "Proper to be bound up therewith"
date: 2021-06-16T09:24:47+01:00
description: 'A talk from SHARP on especially suggestive binding instructions.'
dot: "."
---

Scandal commonly occurs as a selling point in pamphlets, but what’s
unusual about this pamphlet is that it has a strange cover instruction
to be “bound up therewith” that sounds as much about the content of
the poem as the physical item itself.

The talk also highlights the value of teaching from your own collections.

{{< youtube id="I6Wn9zPIS1s" >}}

This talk was given at [SHARP 2021: Moving Texts](https://www.sharpweb.org/movingtexts2021/).
