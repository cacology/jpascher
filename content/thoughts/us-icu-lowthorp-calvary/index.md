---
title: "The the Calvary bookshop’s copy of the Transactions"
date: 2022-09-12T08:20:24-05:00
description: 'The simple reason for a nineteenth-century German binding to be in Chicago'
dot: "."
---

This series of thoughts has been puzzling out John Lowthorp’s
[abridgment of the *Philosophical Transactions of the Royal Society of
London*](/thoughts/us-icu-lowthorp) at he University of Chicago
Library [Hanna Holborn Gray Special Collections Research
Center](https://www.lib.uchicago.edu/scrc/).  We [figured out the
source of the books](/thoughts/us-icu-lowthorp-accession) and [noted
that they had similar bindings.](/thoughts/us-icu-lowthorp-volumes)

The new university President bought the S. Calvary and Company’s
entire book collection to establish the library.  I’m grateful to
[Catherine
Uecker](https://www.lib.uchicago.edu/about/directory/staff/catherine-uecker/)
for pointing out an [online exhibit that explains the
details.](https://www.lib.uchicago.edu/collex/exhibits/extbc/)

The collection included a range of periodicals, but some were still to be bound,

> There were provisions for securing additional scholarly periodicals
> and for the packing and binding of the books, as well as for the
> compilation of a list of all the books purchased ([source](https://www.lib.uchicago.edu/collex/exhibits/extbc/history/calvary-and-company/))

The [longer exhibit catalog](https://www.lib.uchicago.edu/ead/pdf/ex-berlin.pdf) explains that there was a conflict about the terms of the arrangement, but that

> Calvary continued to complete and bind journals for the University [during the disagreement] ([18](https://www.lib.uchicago.edu/ead/pdf/ex-berlin.pdf#page=26))

This finally explains the mystery of both the volume and its binding.
This is the binding made by a nineteenth-century German bookbinder
imagining what the University of Chicago might want: morocco grained,
dark half sheep, with somber and dark marbled boards; the name of the
new university tactfully gold-tooled in the lower-left corner of the
upper cover; the name of the journal displayed on the spine with the
volume number establishing it as one of a long row of *Transactions*
from the Royal Society.  I think I can see that through the collaged
image of what remains of the binding on two different volumes,

![Collaged binding](pieces.jpg)

As for the early state of the
[plates](/thoughts/us-icu-guide-numbers), the binding does not resolve
that, but gives a direction for further investigation.  John
Lowthorp’s abridgment sold out as a subscription.  The only copies
available were either second-hand or resold by distributors who bought
several.  A second edition [appeared around
1719](http://estc.bl.uk/T103702), quickly followed by other editions.
It might be worth looking at old Calvary sale catalogs to see if they
had other editions and where the other volumes of this edition went,
or came from.
