---
title: "2024 ASECS—SHARP Panels"
date: 2024-10-06
description: 'Abstracts and, eventually, the papers for the ASECS-SHARP panel in Toronto.'
private: true
dot: "."
---

Author-focused literary studies have long ruled the roost, but some of
the most important books have not had clear authors. Rather than a
single author, these books have multiple authors, or false authors, or
no author, or we simply do not know who conceived of and wrote
them. Indeed, periodicals, newspapers, menus, folktales, and many
other textual forms central to human activity flourish without
conventional authorship. These paired panels bring together new
thinking that explores books or other textual forms with
overdetermined or under-determined authorship including but not
limited to considering forgeries, anonymous work, pseudonymous work,
openly transitioning authors, collaborative authorship, un-authored
works, works that resist authorship, works that emphasize authorship
alone, or any concept of textual creation that challenges a simplistic
model of authorship.

## Deadlines for pre-circulated papers

**Deluxe deadline, 1 February:** with this deadline, you get feedback and can revise for,

**Regular deadline, 26 February:** with this deadline, the group will read
  your paper in advance of ASECS, but feedback in advance from anyone
  isn’t guaranteed.

Drafts will be posted below.

## Overdetermined Authorship: Rejecting Conventional Authorship (Chair: Betty A. Schellenberg <schellen@sfu.ca>)

<!--### “Cöln, bey Peter Marteau; a Cologn, chez Pierre Marteau; and printed at Cologne by Peter Marteau”: Fictitious Imprints and European Print Culture (Dylan Lewis <dplewis@umd.edu>)

While notions of authorship (whether under or overdetermined) have
been the predominant way book historians have discussed a text’s
legacy, identity, or association, figures such as publisher, printer,
and bookseller also play a large role in a text’s creation and lasting
identity and association, especially when authorship was contested or
unknown. My paper concerns the fictitious imprint of Pierre Marteau,
an alleged publishing house located in Cologne in the seventeenth and
eighteenth centuries.

Starting in the seventeenth century, the Marteau imprint took on a
broad European identity amidst a chaotic and fractured print culture
with original Marteau texts—produced by an assortment of authors,
printers, and publishers—appearing in France, Germany, the
Netherlands, Italy, and England. As politically salacious texts, the
author was almost never identified on a Marteau imprinted work’s title
page. But these texts, instead, can be read as a body of work unified
around the Marteau imprint. Authors, and most certainly readers,
understood that such a publishing house never actually existed, and
the imprint was, instead, used as a kind of ‘brand identity’ that
unified disparate works that were skeptical of the monarchy and
members of various European courts.

In my analysis of the Marteau imprint I discuss: 1) the way the
imprint served as a kind of authorship to unite specific
anti-monarchical political ideologies across a vast and varied
European print culture; 2) the importance of Marteau’s ‘publishing
house’ being in Cologne; and 3) the revival of the Marteau imprint in
the nineteenth century and by contemporary scholars in the twentieth
and twenty-first century.

[Proposal](Dylan%20Lewis%20ASECS%202024%20SHARP%20Panel%20Abstract%20-%20Dylan%20Lewis.pdf)-->

### William Lane’s Minerva Press and the Ever-Malleable Author Function (Yael Shapira <shapira.yael@biu.ac.il>)

Dozens of writers authored novels for William Lane’s Minerva Press,
the publishing house that dominated the production of new fiction in
England at the end of the eighteenth century. As has been amply
discussed, contemporary reviewers were appalled by Lane’s formulaic
fare and refused to treat it as the product of a genuine authorial
act: “Over and over,” Ina Ferris writes in *The Achievement of Literary
Authority*, “the ordinary novel is depicted as stamped out by machines,
produced not by authors but by printing presses. It thus appears as a
discourse outside the author-function identified by Foucault as
fundamental to the category of the literary as it was developed by the
eighteenth and early nineteenth centuries.”

Joining a recent wave of studies on what Jennie Batchelor has called
the “culture of unRomantic authorship,” my paper will look beyond the
dismissive periodical discourse to other ways of assessing how much
weight the idea of the author carried in relation to Minerva’s
fiction. Rather than existing “outside” the author function, I will
argue, Minerva novel-writing was a discursive site within which the
concept of the author proved usefully malleable – especially for Lane
himself. By looking at Lane’s advertisements and publicity pamphlets
of the 1790s, the decade in which Minerva established itself as the
leading force in the popular fiction market, I will show his variable
use of his authors’ Foucauldian “function,” which Lane played up and
down in accordance with his changing marketing strategies. By way of
conclusion, I will consider some examples of how Minerva novelists
negotiated their own uncertain author status, in dialogue with – and
sometimes in covert opposition to – their publisher’s entrepreneurial
efforts.

[Proposal](Shapira%20abstract%20for%20Overdetermined%20and%20Underdetermined%20authorship%20panel%20-%20ASECS%202024%20-%20Yael%20Shapira.docx)

### Accounting for Authorial Labor in Women’s Self-Published Texts of the Long Eighteenth Century (Emily D. Spunaugle <spunaugle@oakland.edu>)

Book history’s corrective to author-centric literary studies shows
that printing and publishing required many hands doing muddled albeit
distinct tasks, as in the work of Helen Smith, Kate Ozment, and Lisa
Maruca. But what of authors who had a hand in the publication of their
own works? What of authors whose labors exceed being merely authorial,
but which are involved in a greater variety of labors? To highlight
the agency of the author, I propose the term author-facilitated
publication to account for the physical, mental, and emotional hustle
required for an author to see their work in print. These authorial
labors are typically obscured—particularly in the case of women. This
paper returns to the bodies of women authors not to reattribute texts
or create a new canon, although this recovery work is
paramount. Instead, this paper avoids the cult of the author-function
created in concert with celebrity and instead uses the examples of
relatively unknown authors to account for their labors in excess of
composition. I use as example the autobiographical descriptions of the
publishing ventures of Scottish author, Jean Marishall (active
1765-1789). I use Marishall’s two-volume *A Series of Letters*
(1788-89), which include detailed accounts of her attempts to publish,
to read for authorial labor in less illustrative cases, including
women’s co-called “vanity publications” of the long eighteenth
century. Ultimately, this paper aims to broaden what counts as labor
in the print trade and offer a new concept, author-facilitated
publication, for investigating women’s print publications.

[Proposal](Spunaugle%20Proposal%20for%20ASECS%202024%20session%2080%20Overdetermined%20and%20Under-determined%20Authorship_%20Methods%20and%20Progress%20in%20Identity%20[Society%20for%20the%20History%20of%20Authorship,%20Reading,%20and%20Publishing%20(SHARP)]%20-%20Emily%20D.%20Spunaugle.docx)

[Draft](Spunaugle_ASECS_2024.docx)

### Scribes as Authors in Restoration Manuscript Culture: The Case of *Sodom* (Jeremy Webster <webstej1@ohio.edu>)

The only known surviving printed copy of *Sodom*, one of the great
authorial puzzles of the Restoration era, was auctioned by Sotheby’s
in 2004 to a private collector for £45,600. The presale catalogue
contains an image of the book’s title page, which attributes the
pornographic work to “the E. of R.,” John Wilmot, Earl of
Rochester. Scholars have long debated this attribution, which looms so
large over the text that it is rarely studied apart from this
controversy.  My paper posits that it is time to decouple the study of
*Sodom*, specifically, and Restoration manuscript satires generally,
from the paradigm of single authorship by taking seriously the
authorial roles of the scribes who produced the manuscript
miscellanies that eventually formed the bases for printed editions in
the early eighteenth century. Indeed, what Matthew Fisher writes of
Medieval scribes is equally true of Restoration ones: “Scribes did
much more than copy the exemplars before them.” As Paul Hammond
similarly suggests, amateur and professional scribes during the
Restoration period not only copied texts but created new ones along
with new frameworks for understanding them. Yet the authorial function
of scribes isre largely absent from our study of what Peter Beal calls
“the last great flourishing of manuscript literary culture in
England.” I argue that studying Sodom as a product of what Fisher
calls “scribal authorship” is an excellent case study for
understanding the under-determined authorial role of scribes in
Restoration manuscript culture. To make this argument, I will outline
the textual development of Sodom from its earliest incarnation (likely
Princeton C0199) to the early eighteenth-century print edition and
show how scribes likely created the text as we now know it. Based on
this example, I further argue for seeing scribal authorship as an
equally (if not more) seminal component in the production and
transition of late seventeenth-century satire from manuscript to
print, from scribal miscellanies to printed works such as *Poems on
Affairs of State*, and for challenging the primacy of single-author
studies.

[Proposal](Webster%20Proposal%20for%20ASECS%202024%20session%2080%20Overdetermined%20and%20Under-determined%20Authorship_%20Methods%20and%20Progress%20in%20Identity%20[Society%20for%20the%20History%20of%20Authorship,%20Reading,%20and%20Publishing%20(SHARP)]%20-%20Jeremy%20Webster.docx)

[Draft](Webster%20ASECS%202024%20paper%20022524.pdf)

## Under-determined Authorship: Complicating Established Authors (Chair: J. P. Ascher <jpa4q@virginia.edu>)

### Maria Edgeworth and the Collaborative Composition of Harry and Lucy Concluded (Susan Egenolf <s-egenolf@tamu.edu>)

Maria Edgeworth’s novels have long been recognized as collaborative
works influenced by her larger family community. Although Richard
Lovell (her father) provided feedback on her fictions and co-wrote
several educational works, including early volumes of *Early Lessons*,
he was not the only person who influenced her writings: her larger
family circle played an active role in the composition and revision of
her novels. I would argue, however, that Edgeworth's composition
process and the content of her *Harry and Lucy Concluded* (the final
four volumes of Early Lessons, 1825) considerably widened the net of
her collaborators.  I am co-editor of the *Maria Edgeworth Letters*
Project and am continuing to find that Edgeworth was pushing
boundaries in her composition by enlisting different inventors and
business entrepreneurs (such as Josiah Wedgwood II, pottery magnate,
and William Strutt, cotton spinner and engineer) to edit and expand
her manuscript pages for *Harry and Lucy*. The final *Harry and
Lucy* volumes, I believe, take her composition practice to the level of
crowdsourcing, well beyond coterie or small-group practices we usually
see in Romanticism.  When writing Wedgwood in 1823, she claimed she
was “ignorant of science” and that the “only way in which I can
venture to finish this little book is by asking the assistance of his
[her father’s] friends in the different scientific parts which they
particularly understand,” instructing him “cut & carve freely” the
included manuscript pages.  *Harry and Lucy* is filled with mechanical
and scientific knowledge as the characters and their parents tour the
industrial Midlands, and Edgeworth, who did know some science, is
marshalling forth scientists, inventors and entrepreneurs to produce
accurate and compelling information that Humphry Davy hoped
would “have the effect of making the rising generation agile thinkers
on subjects, which have not sufficiently occupied the thoughts of
their fathers & grandfathers,” adding, “I am sure all Men of Science
have great obligation to you as their fair advocate.”

[Proposal](Susan%20Egenolf%20ASECS2023.Edgeworth%20and%20the%20Collaborative%20Composition%20of%20Harry%20and%20Lucy%20Concluded.docx)

### Samuel Johnson’s Construction of Shakespeare in *Miscellaneous Observations on the Tragedy of Macbeth* (1745) (Megan E. Fox <megan.e.fox@wisc.edu>)

Twenty years before the publication of his edition of Shakespeare’s
works, Samuel Johnson published the pamphlet *Miscellaneous
Observations on the Tragedy of Macbeth* (1745). Therein, he articulates
his understanding of the relationship between Shakespeare and his
cultural milieu. Johnson, one of a cascade of literary editors of
Shakespeare (including Pope, Theobald, Warburton, and Malone) during
the eighteenth century, articulates his editorial intervention in
contrast to his contemporaries through a specific attention to the
linguistic contours of Shakespeare’s language. Johnson’s focus on
Shakespeare’s language, etymology, and style when editing develops
further in the years between *Miscellaneous Observations* and his
eight-volume edition of the plays (during which time he publishes *A
Dictionary of the English Language*).

In this paper, I examine Johnson’s evolving characterization of
Shakespeare’s language through a close reading of his notes on Macbeth
in Miscellaneous Observations and his later edition of the play. I
argue that Johnson’s attention to language, etymology, and style was a
contributing factor in a larger editorial project during the period
which constructs Shakespeare’s authorship as an overdetermined
category of interpretation. “Shakespeare” comes to represent not just
the poet-playwright, but also becomes a synecdoche for how
eighteenth-century editors understood (and formed) a coherent
conception of early modernity, intellectual and literary taste, and
the English character.

[Proposal](Fox%20ASECS%20Proposal%20%20-%20Megan%20E.%20Fox.pdf)

### Alexander Pope’s Cooperative Satire (Katherine Mannheimer <katherine.mannheimer@rochester.edu>)


As Britain’s first self-supporting poet, Pope is often described as a
poet of the self. Specifically, Pope’s reputation as the era’s
preeminent satirist led to what Helen Deutsch calls an alternating
stance of “self-defense” and “self-exposure,” creating an oeuvre in
which Pope became “his own favorite subject.”

This critical focus on Pope as self-made and self-authoring, however,
neglects the lessons of Margaret Ezell’s depiction of Pope as
participating in the practice of “social authorship,” as well as, more
recently, Joseph Hone’s analysis of Pope’s uses of “scribal
publication,” especially in the first decade of his career: much of
the early work (perhaps most famously The *Rape of the Lock*) circulated
in manuscript prior to print publication.

While Ezell’s and Hone’s scholarship helps us to see Pope and his work
within new literary-historical contexts, Pope Studies has so far been
slow to apply these insights directly to the close interpretation (or
reinterpretation) of individual poems.

In this paper I will read Pope’s *To a Lady* in such a way as to restore
it to its original social setting (the poem notoriously begins, of
course, by quoting a comment that Pope’s friend Martha Blount “once
let fall”); indeed, I read the poem as in many ways about the
collaborative and interactive nature of imagination—as a glimpse into
how Pope wrote to, for, and with the women portrayed in the poem, even
(or especially) those whom he vociferously disdains.

[Proposal](Mannheimer%20Proposal%20for%20ASECS%202024%20session%2080%20Overdetermined%20and%20Under-determined%20Authorship_%20Methods%20and%20Progress%20in%20Identity%20[Society%20for%20the%20History%20of%20Authorship,%20Reading,%20and%20Publishing%20(SHARP)]%20-%20Katherine%20Mannheimer.docx)

### Performing Indeterminacy: Named Names and Impersonated Persons in Blackwood’s *Edinburgh Magazine* (Christine Woody <cmwoody@widener.edu>)

In this paper I propose to explore how the satirical series “Noctes
Ambrosianae,” running in *Blackwood’s Magazine* beginning in the 1820s
and collectively authored for its first 20 numbers, plays with an open
secret about the indeterminacy of authorship in the Romantic
period. This series presents a sort of behind the scenes view of the
production of the periodical, except, crucially, that view is entirely
constructed, being a performance of what writing for the periodical
might be like, rather than true access to its production
history. Looking specifically at how the text handles two crucial
figures of Scottish authorship in its historical moment—Walter Scott
and James Hogg—I will argue that this series supports the claim that
authorship is being cultivated as an intentionally indeterminate
category within the context of this collectively authored series and
persona/pseudonym-driven periodical. Scott is mentioned almost
constantly in the series, his authorial status highlighted and
reinforced through a multiplication of nicknames—some standard, like
the Author of Waverley, some obscure, like the Great Magician. Hogg,
meanwhile, appears “in person” in the series, although not always
actually written by the author James Hogg. Hogg is encountered not
through his honors and titles but through his works—which are
excerpted, critiqued, and insulted by the other characters—and his
behaviour—often comprised of drinking, singing, and occasionally
passing out. The reader is left to reconcile which version of an
author is real, and to confront the fundamental instability of
authorial brands and personae as staged by this
periodical. Methodologically, this argument draws from insights gained
through the process of encoding this text for a digital edition, and
my presentation will touch on why digital humanities methods can
enrich our approach to texts that challenge the named authorship
model.

[Proposal](Woody%20-%20Proposal%20for%20overdetermined%20and%20under-determined%20authorship%20ASECS%202024%20-%20Christine%20Woody.docx)
