---
title: "Making tools as thinking"
date: 2022-06-20T09:07:16+01:00
description: 'Making our own tools can helps us to ask the right questions.'
dot: "."
---

The watchmaker George Daniels wrote about how an apprentice couldn’t
really use a particular tool until they had made a version on their
own.  One simply didn’t understand the knowledge embodied in subtle
shapes until you had to solve the same problem.

Justice Anthony Kennedy supposedly asks each of his clerks to draft an
opinion independently, while he drafts his own.  He told Bryan Garner
that one cannot see the problems, or the right solutions, to writing
an opinion unless you try it yourself.

I think that this idea is correct: You have to make a tool before you
can use a tool.  Sometimes, your tool is better, other times you’re
ready to use the standard one.

As a scholar of the history of texts, I spend a lot of time
transcribing.  In 2017, I [wrote a long piece on
it.](https://scholarslab.lib.virginia.edu/blog/transcribing-typography-with-markdown/)
I have, however, continued to think about the problem and I was happy
to see how much that early version had influenced my thinking.

A text is a sequence of symbols representing a series of decisions by
someone in the past.  To transcribe is to recover those decisions, as
best we can, in the present and represent them in the present.  How do
you do this when we type one way and the past may have written a whole
other way?

You do this by looking at lots of texts, trying to describe them, and
adjusting.  My current thinking has become a small format I maintain
for all my work, [JPDoc.](https://gitlab.com/cacology/jpdoc/) I call
it after myself not out of arrogance, but to say that this isn’t a
general solution.  This is *my* solution.  You should copy it and
break it, adapt it, make it your own, so that you understand the
problems involved.

Then you can come back and decide if I did it right.
