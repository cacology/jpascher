---
title: "Commonplacing as banking"
date: 2022-06-28T08:03:25+01:00
description: 'Seventeenth century antiquarians engaged in commonplacing following accounting principles.'
image: "SlCancel.jpg"
dot: "."
---

Seventeenth-century commonplace books and recipe books often have
partial indexes.  The compiler would collect new prescriptions,
descriptions of books, good ideas, or observations.  And then, at some
later date, they would go back through their collection and make a
topical index.

Contemporary writers, including John Locke, describe indexing like
this in instructions for making commonplace books.  What I’ve been
thinking about is how people marked their entries as entered into
their index.  You can see check marks, crosses, and slashes through
entries entirely to indicate they’ve been indexed.

Above you can see John Bagford’s commonplace book ([Uk Sloane 893
(39)](Sloane893.39.JPG)), in which he uses the slash method.  He wrote
entries about books he saw in his waste book ([Uk Sloane
923](Sloane923.JPG)), which he copied into this commonplace book
above.  He also uses crosses and marks along the margin to indicate he
has indexed the item.

His slash, however, seems to indicate not that the entry has been
indexed, but that it has been transferred.  This is an established
convention in Venetian double-entry bookkeeping.

Luigi Pacioli writes about a merchant keeping a memorandum book, or
waste book, in which they record their day-to-day activities.  At the
end of the day when they have time, they write their waste book
entries more clearly in their journal and mark them out with a slash.
The slash prevents them from copying the entry twice, which would
disrupt their accounting.

Similarly, as the entries in the journal book accumulate, the merchant
would transcribe entries into the ledger book.  The entries in the
journal book would be marked through with a slash too to indicate that
they had been transferred.

As Pacioli writes, you can recreate all three books from either the
complete waste book or the complete journal book.  You can also check
your ledgers, find errors, or find entries you missed.
Establishing a standard system of marking means you can audit your
books.

Here, however, we see John Bagford using the same model: one entry is
transferred from the waste book to the journal and from the journal to
a ledger book (now lost, but which was transcribed by a clerk into [Uk
Sloane 1085](Sloane1085.11.JPG)).

This is writing as banking.  Bagford, like many antiquarians and
virtuosi of the time, used general accounting methods to keep his own
notes straight.  The slash indicates that an entry has been copied to
the next book, but does not obscure what it says.  This way, you can
refer to your old notes, but feel secure that you’ve copied them into
your next project.

Now we can still go back and check his notes.
