---
title: 'About'
image: images/side-eye.jpg
menu:
  main:
    name: "About"
dot: "."
---

J. P. Ascher studies bibliographical methods along with Restoration to
eighteenth-century generalist writing. He is currently working on two
book projects: first, a broad history of seventeenth-century reading
titled “Creating an Audience for Science: The Philosophical
Transactions of the Royal Society of London in Context,” and, second,
a comprehensive “Descriptive Bibliographical History of Henry
Oldenburg’s Editorship of the Philosophical Transactions, 1665-1676.”
Over the summer, he conducted primary-source research at the Royal
Society with a fellowship from the Bibliographical Society of America,
the Katherine F. Pantzer Jr. Scholarship from the Bibliographical
Society, and as a Platzman Fellow at the University of Chicago.

This fall he will be the inaugural Presidential Scholar in
Bibliography at the Linda Hall Library, the Pantzer Fellow in
Descriptive Bibliography at the Houghton Library, and taking up a Lisa
Jardine Grant to continue his work at the Royal Society in London.

His [dissertation](https://doi.org/10.18130/jqpe-zc65) was a
descriptive bibliographical history of the very beginning of
enlightened reading epitomized by the periodical *The Philosophical
Transactions* of the Royal Society of London.

## Where is he now?

- London, UK (7 May to 22 July 2022)
- Charlottesville, VA (23 July to 9 Aug 2022)
- Chicago, IL (10 Aug to 1 Oct 2022)
- Durham, NC (2 Oct to 18 Nov 2022)
- Kansas City, MO (22 Nov 2022 to 30 April 2023)
- Lawrence, KS (1 May to 6 May)
- Murfreesboro, TN (7 May to 13 May)
- Bloomington, IN (14 May to 21 May)
- Charlottesville, VA  and Durham, NC (22 May to 26 May)
- Cambridge, MA (27 May to 2 Oct)
- London, UK (2 Oct to 8 Nov)
- Portland, OR (8 Nov to 12 Nov)
- Melbourne, FL (13 Nov to 20 Nov)
- Edinburgh, UK (21 Nov to 3 Jan)
- San Francisco, CA (3 Jan to 8 Jan)
- Edinburgh, UK (8 Jan)
